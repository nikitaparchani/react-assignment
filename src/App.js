import React from "react";
import "./App.css";
import Search from "./component/Search";
import { Container, Row, Col } from "react-bootstrap";
import HeaderData from "./component/HeaderData";
function App() {
  return (
    <div className="App">
      <HeaderData />
      <Container>
        <Row>
          <Col>
            <Search />
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default App;
