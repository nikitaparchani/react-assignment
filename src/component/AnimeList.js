import React, { Component } from "react";

export default function AnimeList(props) {
  var error = props.error;
  var isLoaded = props.isLoaded;
  var items = props.items;
  var handleDataClick = props.handleData;
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <div className="listContainer">
        <ul className="displayFlex listDivision">
          {items.map((item) => (
            <li className="listData" key={item.title}>
              <img src={item.image_url} />
              <p className="alignCenter titleText">{item.title}</p>
            </li>
          ))}
        </ul>
        <div>
          <a className="loadMore " onClick={props.handleData}>
            Load More.....
          </a>
        </div>
      </div>
    );
  }
}
