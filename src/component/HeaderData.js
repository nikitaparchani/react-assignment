import React, { Component } from "react";
export default function HeaderData(props) {
  return (
    <header className="navHeader">
      <nav>
        <div className="menu-icon"></div>
        <ul className="list">
          <li>
            <a className="nav-link" href="#">
              Document
            </a>
          </li>
          <li>
            <a className="nav-link" href="#">
              Showcase
            </a>
          </li>
          <li>
            <a className="nav-link" href="#">
              FAQs
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
}
