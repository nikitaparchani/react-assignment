import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import AnimeList from "./AnimeList";
export default class search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      value: "",
      count: 0,
      url: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleData = this.handleData.bind(this);
  }
  handleChange(event) {
    if (event.target.value) {
      this.setState({ value: event.target.value });
    } else {
      this.setState({ isLoaded: false });
    }
  }
  handleData() {
    var countValue = ++this.state.count;
    this.setState({ count: countValue });
    this.handleClick(this.state.count);
  }
  handleClick = (page, event) => {
    if (this.state.count == 0) event.preventDefault();
    var value = this.state.value;
    if (value) {
      fetch(
        "https://api.jikan.moe/v3/search/anime?q=" +
          value +
          "&limit=16&page=" +
          page
      )
        .then((res) => res.json())
        .then(
          (result) => {
            this.setState((prevState) => ({
              isLoaded: true,
              items: [...prevState.items, ...result.results],
              url:
                "https://api.jikan.moe/v3/search/anime?q=" +
                value +
                "&limit=16&page=" +
                page,
            }));
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            this.setState({
              isLoaded: true,
              error,
            });
          }
        );
    }
  };
  render() {
    return (
      <div>
        <div className="searchPosition">
          <Form
            className="displayFlex"
            onSubmit={(event) => this.handleClick(1, event)}
          >
            <Form.Control
              size="lg"
              type="text"
              value={this.state.value}
              onChange={this.handleChange}
              className="searchtext"
            />
            <Button type="submit" className="submitBtn">
              GO
            </Button>
          </Form>
        </div>

        {this.state.isLoaded && (
          <div>
            <p className="requesturl">
              Requesting: <span className="url">{this.state.url}</span>
            </p>
            <AnimeList
              error={this.state.error}
              isLoaded={this.state.isLoaded}
              items={this.state.items}
              handleData={this.handleData}
            />
          </div>
        )}
      </div>
    );
  }
}
